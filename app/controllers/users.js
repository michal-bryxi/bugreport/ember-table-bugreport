import Controller from "@ember/controller";

export default class UsersController extends Controller {
  filter = 'Initial filter';
  queryParams = ["filter"];
  columns = [
    {
      name: `Name`,
      valuePath: `name`,
    },
  ];
}
