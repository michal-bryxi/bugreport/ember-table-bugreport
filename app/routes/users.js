import Route from "@ember/routing/route";

export default class UsersRoute extends Route {
  queryParams = {
    filter: {
      refreshModel: true
    }
  };

  model() {
    return [
      {name: 'John'},
      {name: 'Jane'},
      {name: 'FooBar'}
    ];
  }
}
